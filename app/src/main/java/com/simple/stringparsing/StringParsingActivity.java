package com.simple.stringparsing;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class StringParsingActivity extends AppCompatActivity {

    EditText input;
    TextView output;
    Button btn1, btn2;
    String inputSt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_string_parsing);

        input = findViewById(R.id.text_input);
        output = findViewById(R.id.text_output);
        btn1 = findViewById(R.id.btn1);
        btn2 = findViewById(R.id.btn2);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputSt = input.getText().toString();
                String[] words = inputSt.split("[^a-zA-Z]");
                String[] sortedArray = bubbleSort(words);
                String longestWord = sortedArray[0];
                for (int i = 1; i < sortedArray.length; i++) {
                    if (sortedArray[i].length() > longestWord.length())
                        longestWord = sortedArray[i];
                }
                output.setText(String.format(getString(R.string.longest_word_output), longestWord));
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputSt = input.getText().toString();
                String[] digits = inputSt.split("\\D");
                int sum = 0;
                for (String digit : digits)
                    if (!digit.equals("")) sum += Integer.parseInt(digit);
                output.setText(String.format(getString(R.string.sum_output), String.valueOf(sum)));
            }
        });
    }

    String[] bubbleSort(String[] array) {
        boolean sorted = false;
        String temp;
        while (!sorted) {
            sorted = true;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i].length() > array[i + 1].length()) {
                    temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;
                    sorted = false;
                }
            }
        }
        return array;
    }
}
